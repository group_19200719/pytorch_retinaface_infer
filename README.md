# Описание

Репозиторий содержит `python-package` c удобными классами для инференса модели `RetinaFace in PyTorch`([ссылка на оригинальный github-repo](https://github.com/biubug6/Pytorch_Retinaface)).
Код авторов был написан для реализации научной статьи [RetinaFace: Single-stage Dense Face Localization in the Wild](https://arxiv.org/abs/1905.00641).

При инференсе используется backbone `Resnet50`.

## Результаты полученные авторами статьи на WiderFace Val
| Style | easy | medium | hard |
|:-|:-:|:-:|:-:|
| Pytorch (same parameter with Mxnet) | 94.82 % | 93.84% | 89.60% |
| Pytorch (original image scale) | 95.48% | 94.04% | 84.43% |

## Установка python-пакета в окружение poetry

```
poetry add git+"https://gitlab.com/group_19200719/pytorch_retinaface_infer.git"
```

После этого в `pyproject.toml` появится запись
```
[tool.poetry.dependencies]
emotion-det-net-infer = {git = "https://gitlab.com/group_19200719/pytorch_retinaface_infer.git"}
```

## Классы модуля "emotion_det_net_infer"

При использовании любого из классов указанных ниже будет скачена готовая модель `RetinaFace` из `Google Drive`.

Модель скачивается в каталог `./RetinaFace_weights/Resnet50_Final.pth`.

### Класс `FaceDetector` предназначенный для детекции человеческих лиц с помощью `RetinaFace`.

И так у нас есть тестовая картинка с 4 лицами актрисы №24 из датасета `RAVDESS`([ссылка](https://zenodo.org/record/1188976#.YFZuJ0j7SL8)).

И мы хотим детектировать все лица на данном изображении.

![тестовое изображение](test_pics/frame_0.jpg)

Пример, использования `FaceDetector` показан в `examples/make_face_detection_on_photo.py`.

```(python)
from emotion_det_net_infer import FaceDetector

if __name__ == "__main__":
    # отдаем картинку с 4 лицами актрисы №24 из датасета RAVDESS
    image_path = "test_pics/frame_0.jpg"
    
    # filename для фото с визуализацией детекций
    output_img_name = "face_detection.jpg"

    # инициализируем детектор лиц RetinaFace
    facedet = FaceDetector()
    
    # загружаем картинку
    image = facedet.load_image(image_path)
    
    # детектируем все лица на картинке
    face_detections = facedet.detect_faces(image)
    
    # визуализируем bounding boxes для всех найденных лиц
    image = facedet.viz_detections(image, face_detections)
    
    # сохраняем изображение с детекцией лиц
    image.save(output_img_name)
```

Получаем изображение с bounding boxes для всех найденных лиц.

![детекция лиц](face_detection.jpg)

### Класс `DetectEmotionOnFaces`:
* детектирует лица людей на фото с помощью `RetinaFace`
* передает найденные лица модели классификации эмоций человека, - emotion_net.
* получает эмоции для каждого crop'а с лицом

И так у нас есть тестовая картинка с 4 лицами актрисы №24 из датасета `RAVDESS`([ссылка](https://zenodo.org/record/1188976#.YFZuJ0j7SL8)).

И мы хотим детектировать все лица на данном изображении.

![тестовое изображение](test_pics/frame_0.jpg)

Пример, использования `DetectEmotionOnFaces` показан в `examples/make_emotion_recognition_on_photo.py`.

```
# Инференс модели
import os

from emotion_det_net_infer import DetectEmotionOnFaces
from emotion_net_infer import EmotionSeqClassifier

if __name__ == "__main__":
    # картинка на которой распознаем эмоции людей
    path_to_image = "test_pics/frame_0.jpg"
    
    # filename для фото с визуализацией эмоций
    output_img_name = "face_emotion_recognition.jpg"

    # инициализируем трансформер ITF
    # см. https://gitlab.com/group_19200719/multimodal-emotion-recognition_infer
    emo_seq_classifier = EmotionSeqClassifier()
    
    # инициализируем детектор лиц с распознаванием эмоций на них
    emotion_det_net = DetectEmotionOnFaces(emo_seq_classifier)

    # детектируем лица на фото
    detections, image = emotion_det_net.load_image_and_detect_faces(path_to_image)
    
    # предсказываем эмоции для каждого bbox с лицами, отображаем на фото
    image = emotion_det_net.viz_emo_detections(image, detections)

    # сохраняем результат
    image.save(output_img_path)
```

Получаем изображение с bounding boxes для всех найденных лиц и подписями с распознанными эмоциями.

![распознавание эмоций](face_emotion_recognition.jpg)

# TODO

* [x] Уменьшение картинки до максимально разрешенного размера, если она его превышает.
  (Готово, размер установлен в 760 пикс, потребляемая память ГПУ - 3.5 ГБ)