# Инференс модели
import os

from emotion_det_net_infer import DetectEmotionOnFaces
from emotion_net_infer import EmotionSeqClassifier

if __name__ == "__main__":
    # картинка на которой распознаем эмоции людей
    path_to_image = "test_pics/frame_0.jpg"
    
    # папка для сохранения результатов
    output_folder = "output_preds"
    
    # filename для фото с визуализацией эмоций
    output_img_name = "test_emotion_recognition.jpg"

    # инициализируем трансформер ITF
    # см. https://gitlab.com/group_19200719/multimodal-emotion-recognition_infer
    emo_seq_classifier = EmotionSeqClassifier()
    
    # инициализируем детектор лиц с распознаванием эмоций на них
    emotion_det_net = DetectEmotionOnFaces(emo_seq_classifier)

    # детектируем лица на фото
    detections, image = emotion_det_net.load_image_and_detect_faces(path_to_image)
    
    # предсказываем эмоции для каждого bbox с лицами, отображаем на фото
    image = emotion_det_net.viz_emo_detections(image, detections)

    # сохраняем результат
    output_img_path = os.path.join(output_folder, output_img_name)
    image.save(output_img_path)
